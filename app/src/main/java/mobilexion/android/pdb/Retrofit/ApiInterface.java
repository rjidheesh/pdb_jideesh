package mobilexion.android.pdb.Retrofit;


import com.google.gson.JsonObject;

import java.util.List;

import io.reactivex.Observable;
import mobilexion.android.pdb.Pojo.Episode;
import mobilexion.android.pdb.Pojo.InvoiceReceipt;
import mobilexion.android.pdb.Pojo.Patient;
import mobilexion.android.pdb.Pojo.Status;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ApiInterface {

    @FormUrlEncoded
    @POST("pdb/verify-mobile")
    Call<Status> verifyMobile(@Field("mobileNo") String mobileNo);

    @FormUrlEncoded
    @POST("pdb/patient-details")
    Observable<Patient> getPatient(@Field("mobileNo") String mobileNo);

    @FormUrlEncoded
    @POST("pdb/episode-details")
    Observable<List<Episode>> getEpisode(@Field("mongoId") String mongoId);

    @FormUrlEncoded
    @POST("pdb/invoice-details")
    Call<List<InvoiceReceipt>> getInvoice(@Field("mongoId") String mongoId);

    @FormUrlEncoded
    @POST("pdb/receipt-details")
    Call<List<InvoiceReceipt>> getReceipt(@Field("mongoId") String mongoId);

    @POST("pdb/get-url")
    Call<JsonObject> getVcURL();


}
