package mobilexion.android.pdb;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import mobilexion.android.pdb.Fragments.MobileFragment;
import mobilexion.android.pdb.Utility.Util;

import static mobilexion.android.pdb.Utility.Util.FLAG;
import static mobilexion.android.pdb.Utility.Util.PERMISSIONS;

public class LoginActivity extends AppCompatActivity {

    Util util;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_login);

        util = new Util(this);

        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if(currentapiVersion > 22){

            int PERMISSION_ALL = 1;

            if(!hasPermissions(this, PERMISSIONS)) {
                ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
            }
        }

        if (Util.myPreference.getBoolean(FLAG, false)) {
            startActivity(new Intent(this, MainActivity.class));
            finish();
        }


        if (savedInstanceState == null) {
            Fragment fragInstance;

            //Calling the Fragment newInstance Static method
            fragInstance = new MobileFragment();

            getSupportFragmentManager().beginTransaction()
                    .add(R.id.login_fragment_conatiner, fragInstance)
                    .commit();
        }

        //setFragment(new MobileFragment());
    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        if(getFragmentManager().getBackStackEntryCount() == 0) {
            super.onBackPressed();
        }else {
            getFragmentManager().popBackStack();
        }
    }



}
