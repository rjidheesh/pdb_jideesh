package mobilexion.android.pdb.Pojo;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import java.io.Serializable;

@Entity(tableName = "Episode")
public class Episode implements Serializable {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "mongoId")
    private String mongoId;

    @ColumnInfo(name = "mongoIdPatient")
    private String mongoIdPatient;

    @ColumnInfo(name = "episode")
    private String episode;

    @ColumnInfo(name = "mongoIdConsultant")
    private String mongoIdConsultant;

    @ColumnInfo(name = "specialty")
    private String specialty;

    @ColumnInfo(name = "roomId")
    private String roomId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMongoId() {
        return mongoId;
    }

    public void setMongoId(String mongoId) {
        this.mongoId = mongoId;
    }

    public String getMongoIdPatient() {
        return mongoIdPatient;
    }

    public void setMongoIdPatient(String mongoIdPatient) {
        this.mongoIdPatient = mongoIdPatient;
    }

    public String getEpisode() {
        return episode;
    }

    public void setEpisode(String episode) {
        this.episode = episode;
    }

    public String getMongoIdConsultant() {
        return mongoIdConsultant;
    }

    public void setMongoIdConsultant(String mongoIdConsultant) {
        this.mongoIdConsultant = mongoIdConsultant;
    }

    public String getSpecialty() {
        return specialty;
    }

    public void setSpecialty(String specialty) {
        this.specialty = specialty;
    }

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }
}
