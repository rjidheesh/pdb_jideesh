package mobilexion.android.pdb.Pojo;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import java.io.Serializable;

@Entity(tableName = "Menu")
public class Menu implements Serializable {

    public Menu(String item, int icon) {
        this.item = item;
        this.icon = icon;
    }

    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "item")
    private String item;

    @ColumnInfo(name = "icon")
    private int icon;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }
}
