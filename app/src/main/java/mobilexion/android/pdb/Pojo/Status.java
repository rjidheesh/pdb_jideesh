package mobilexion.android.pdb.Pojo;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Status {

    @SerializedName("status")
    public String status;

    @SerializedName("otp")
    public String otp;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        status = status;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }
}
