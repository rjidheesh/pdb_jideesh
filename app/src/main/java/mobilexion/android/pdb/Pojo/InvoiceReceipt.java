package mobilexion.android.pdb.Pojo;

import com.google.gson.annotations.SerializedName;

public class InvoiceReceipt {

    @SerializedName("mongoId")
    public String mongoId;

    @SerializedName("episode")
    public String episode;

    @SerializedName("invoiceNo")
    public String invoiceNo;

    @SerializedName("invoice")
    public String invoice;

    @SerializedName("mode")
    public String mode;

    @SerializedName("item")
    public String item;

    @SerializedName("date")
    public String date;

    @SerializedName("amount")
    public String amount;

    @SerializedName("details")
    public String details;

    public String getMongoId() {
        return mongoId;
    }

    public void setMongoId(String mongoId) {
        this.mongoId = mongoId;
    }

    public String getEpisode() {
        return episode;
    }

    public void setEpisode(String episode) {
        this.episode = episode;
    }

    public String getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    public String getInvoice() {
        return invoice;
    }

    public void setInvoice(String invoice) {
        this.invoice = invoice;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }
}
