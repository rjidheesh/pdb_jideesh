package mobilexion.android.pdb.Utility;

import android.support.annotation.NonNull;
import android.util.Log;

import androidx.work.Worker;

import java.io.IOException;
import java.util.List;

import mobilexion.android.pdb.Pojo.InvoiceReceipt;
import mobilexion.android.pdb.Retrofit.ApiClient;
import mobilexion.android.pdb.Retrofit.ApiInterface;
import retrofit2.Call;
import retrofit2.Response;

import static mobilexion.android.pdb.Utility.Util.MONGO_ID;

public class PaymentWorker extends Worker {

    public static final String WORK_DATA = "data";

    @NonNull
    @Override
    public Result doWork() {

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        String mongoId = Util.myPreference.getString(MONGO_ID, "NA");
        Call<List<InvoiceReceipt>> call = apiService.getInvoice(mongoId);
        Response<List<InvoiceReceipt>> response = null;
        try {
            response = call.execute();
        } catch (IOException e) {
            e.printStackTrace();
            Log.d("TTT", e.toString());
        }
        if (response.code() == 200) {
            List<InvoiceReceipt> listData = response.body();
        } else {
            return Result.RETRY;
        }

        return Result.SUCCESS;
    }
}
