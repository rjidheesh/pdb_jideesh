package mobilexion.android.pdb.Utility;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import java.util.List;

import androidx.work.Data;
import androidx.work.Worker;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import mobilexion.android.pdb.Database.DBMyDatabase;
import mobilexion.android.pdb.Pojo.Episode;
import mobilexion.android.pdb.Pojo.Patient;
import mobilexion.android.pdb.Retrofit.ApiClient;
import mobilexion.android.pdb.Retrofit.ApiInterface;

import static mobilexion.android.pdb.MainActivity.sMobileString;
import static mobilexion.android.pdb.Utility.Util.MOBILE;
import static mobilexion.android.pdb.Utility.Util.MONGO_ID;

public class PatientWorker extends Worker {

    CompositeDisposable compositeDisposable = new CompositeDisposable();
    public static final String WORK_DATA = "data";

    int retryCount=0;

    @NonNull
    @Override
    public Result doWork() {

        final Result[] result = {Result.SUCCESS};

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        String mobile = Util.myPreference.getString(MOBILE, "NA");
        if (mobile.equals("NA")) mobile = sMobileString;

        compositeDisposable.add(apiService.getPatient(mobile).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<Patient>() {
                           @Override
                           public void accept(Patient patient) throws Exception {
                               if (!patient.getName().equals("no-data-found")) {
                                   Util.mMyDatabase.dbDao().addPatient(patient);
                                   createPref(patient.getMongoId(), patient.getMobileNo());
                               } else Toast.makeText(getApplicationContext(), "no-data-found",
                                       Toast.LENGTH_SHORT).show();
                           }
                       }, new Consumer<Throwable>() {
                           @Override
                           public void accept(Throwable throwable) throws Exception {
                               Toast.makeText(getApplicationContext(), "Error- " + throwable.getMessage(), Toast.LENGTH_LONG).show();
                               result[0] = Result.RETRY;
                               retryCount++;
                               if (retryCount>3)
                                   result[0] = Result.FAILURE;
                           }
                       }
                ));
        Data output = new Data.Builder()
                .putString(WORK_DATA, Integer.toString(result[0].ordinal()))
                .build();

        setOutputData(output);

        return result[0];
    }

    void createPref(String mongoId, String mobile) {
        SharedPreferences.Editor editor = Util.myPreference.edit();
        editor.putString(MONGO_ID, mongoId);
        editor.putString(MOBILE, mobile);
        editor.commit();
    }
}
