package mobilexion.android.pdb.Utility;

import android.Manifest;
import android.app.Activity;
import android.arch.persistence.room.Room;
import android.content.SharedPreferences;

import mobilexion.android.pdb.Database.DBMyDatabase;
import mobilexion.android.pdb.R;

import static android.content.Context.MODE_PRIVATE;

public class Util {
    public static DBMyDatabase mMyDatabase;
    public static SharedPreferences myPreference;

    public static final String MONGO_ID = "mongoId";
    public static final String MOBILE = "mobile";
    public static final String FLAG = "flag";

    public static final String[] PERMISSIONS = {Manifest.permission.CAMERA, Manifest.permission.MODIFY_AUDIO_SETTINGS,
            Manifest.permission.RECORD_AUDIO, Manifest.permission.READ_SMS, Manifest.permission.RECEIVE_SMS,
            Manifest.permission.READ_PHONE_STATE};

    public static final String[] ITEM_NAME = {"Records", "Lab Result", "Medication", "Ascultation",
            "ECG"};
    public static final int[] ITEM_LOGO = {R.drawable.icons_mrecords, R.drawable.icons_lab_result,
            R.drawable.icons_medication, R.drawable.icons_steth, R.drawable.icons_ecg};

    public Util(Activity mContext) {
        this.mMyDatabase = Room.databaseBuilder(mContext, DBMyDatabase.class,"PDBdb").
                allowMainThreadQueries().build();

        myPreference = mContext.getSharedPreferences("pdbPreference", MODE_PRIVATE);
    }
}
