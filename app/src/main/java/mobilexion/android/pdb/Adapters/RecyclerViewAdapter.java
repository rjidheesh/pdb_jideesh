package mobilexion.android.pdb.Adapters;

import android.app.Activity;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import mobilexion.android.pdb.Pojo.Menu;
import mobilexion.android.pdb.R;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {

    Activity mContext;
    List<Menu> menuList;

    public RecyclerViewAdapter(Activity mContext, List<Menu> menuList) {
        this.mContext = mContext;
        this.menuList = menuList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.card_recyclerview, viewGroup, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int i) {
        //final MyListData myListData = listdata[position];
        holder.textView.setText(menuList.get(i).getItem());
        holder.imageView.setImageResource(menuList.get(i).getIcon());

        switch (oddEven(i)) {
            case "even":holder.cardView.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorPrimaryDark));
                break;
            case "odd":holder.cardView.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
                break;
        }
    }

    @Override
    public int getItemCount() {
        return menuList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView imageView;
        public TextView textView;
        public ConstraintLayout cardView;
        public ViewHolder(View view) {
            super(view);
            this.imageView =  view.findViewById(R.id.card_imageview_item);
            this.textView =  view.findViewById(R.id.card_textview_item);
            this.cardView = view.findViewById(R.id.constraint_layout);
        }
    }

    String oddEven(int n) {
        if ((n % 2) == 0) return "even";
        else  return "odd";
    }
}
