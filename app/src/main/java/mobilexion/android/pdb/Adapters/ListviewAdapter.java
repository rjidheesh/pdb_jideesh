package mobilexion.android.pdb.Adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import mobilexion.android.pdb.Fragments.PersonalFragment;
import mobilexion.android.pdb.Pojo.InvoiceReceipt;
import mobilexion.android.pdb.R;
import mobilexion.android.pdb.Utility.Util;

public class ListviewAdapter extends BaseAdapter {

    Activity mContext;
    TextView mEarnTypeTextView, mDescriptionTextView, mAmountTextView, mItemNameCheckedTextView,
            mListSnoTextView, mListItemModeTextView, mListDetailsTextView, mListAmountTextView,
            mListDateTextView;
    ImageView mItemImageView;
    CheckBox mItemCheckBox;

    String mDirection;
    public static ArrayList<String> itemNameList;
    public static ArrayList<Integer> itemIconList;
    List<InvoiceReceipt> listData;

    public ListviewAdapter(Activity mContext, String mDirection, List<InvoiceReceipt> listData) {
        this.mContext = mContext;
        this.mDirection = mDirection;
        this.listData = listData;
    }

    @Override
    public int getCount() {
        if (this.mDirection.equals("1")) return 3;
        else if (this.mDirection.equals("2")) return Util.ITEM_NAME.length;
        else return this.listData.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {

        LayoutInflater inflater = LayoutInflater.from(this.mContext);

        switch (this.mDirection) {
            case "1":
                view = inflater.inflate(R.layout.card_listview_invoice, viewGroup, false);
                mEarnTypeTextView = view.findViewById(R.id.card_textview_earn_type);
                mDescriptionTextView = view.findViewById(R.id.card_textview_description);
                mAmountTextView = view .findViewById(R.id.card_textview_amount);

                mEarnTypeTextView.setText("Service");
                mDescriptionTextView.setText("Consultation fee");
                mAmountTextView.setText("250");
                break;
            case "2":

                itemNameList = new ArrayList<String>();/*|*/itemNameList.clear();
                itemIconList = new ArrayList<Integer>()/*|*/;itemIconList.clear();

                view = inflater.inflate(R.layout.card_listview_selection, viewGroup, false);
                mItemNameCheckedTextView = view.findViewById(R.id.card_textview_selection);
                mItemImageView = view.findViewById(R.id.card_imageview_selection);
                mItemCheckBox = view.findViewById(R.id.card_checkbox_selection);

                mItemCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {
                        if (checked) {itemNameList.add(Util.ITEM_NAME[i]); itemIconList.add(Util.ITEM_LOGO[i]);}
                        else {itemNameList.remove(Util.ITEM_NAME[i]); itemIconList.remove(Util.ITEM_LOGO[i]);}
                    }
                });

                mItemImageView.setImageResource(Util.ITEM_LOGO[i]);
                mItemNameCheckedTextView.setText(Util.ITEM_NAME[i]);
                break;
            case "3":
                view = inflater.inflate(R.layout.card_listview_payslip, viewGroup, false);
                mListSnoTextView = view.findViewById(R.id.card_textview_sno);
                mListItemModeTextView = view.findViewById(R.id.card_textview_mode_item);
                mListDetailsTextView = view.findViewById(R.id.card_textview_details);
                mListAmountTextView = view.findViewById(R.id.card_textview_amount);
                mListDateTextView = view.findViewById(R.id.card_textview_date);

                mListSnoTextView.setText(Integer.toString(i+1));
                if (PersonalFragment.flag.equals("Invoice")) mListItemModeTextView.setText(listData.get(i).getItem());
                else  mListItemModeTextView.setText(listData.get(i).getMode());
                mListDetailsTextView.setText(listData.get(i).getDetails());
                mListAmountTextView.setText(listData.get(i).getAmount());
                mListDateTextView.setText(listData.get(i).getDate());
        }

        return view;
    }
}
