package mobilexion.android.pdb;

import android.Manifest;
import android.arch.lifecycle.Observer;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.work.Constraints;
import androidx.work.NetworkType;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import androidx.work.WorkStatus;
import mobilexion.android.pdb.Fragments.AboutFragment;
import mobilexion.android.pdb.Fragments.PersonalFragment;
import mobilexion.android.pdb.Utility.PatientWorker;
import mobilexion.android.pdb.Utility.Util;

import static mobilexion.android.pdb.Utility.PatientWorker.WORK_DATA;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    Toolbar toolbar;
    public static String sMobileString;
    Util util;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        sMobileString = getIntent().getStringExtra("MOBILE");

        util = new Util(this);
        int patientCount = util.mMyDatabase.dbDao().getCountPatient();

        if (patientCount > 0) setMainView(savedInstanceState);
        else makeNetworkCall(savedInstanceState);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
/*
************************************************** */

        //setFragment(new PersonalFragment());
    }

    void setMainView(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            Fragment fragInstance;

            //Calling the Fragment newInstance Static method
            fragInstance = new PersonalFragment();

            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment_container, fragInstance)
                    .commit();
        }
    }

    void makeNetworkCall(final Bundle savedInstanceState) {

        Constraints constraints = new Constraints.Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED)
                .build();

        final OneTimeWorkRequest requestPatient = new OneTimeWorkRequest.Builder(PatientWorker.class)
                .setConstraints(constraints)
                .build();

        WorkManager.getInstance().enqueue(requestPatient);

        WorkManager.getInstance().getStatusById(requestPatient.getId())
                .observe(this, new Observer<WorkStatus>() {
                    // Do something with the status
                    @Override
                    public void onChanged(WorkStatus workStatus) {
                        if (workStatus != null && workStatus.getState().isFinished()) {
                            String data = workStatus.getOutputData().getString(WORK_DATA, "NA");
                            if (data.equals("0")) setMainView(savedInstanceState);
                            else if (data.equals("1")) makeNetworkCall(savedInstanceState);
                        }
                    }
                });
    }

    private void setFragment(Fragment fragment) {
        this.getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, fragment)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if(getFragmentManager().getBackStackEntryCount() == 0) {
                super.onBackPressed();
            }else {
                getFragmentManager().popBackStack();
            }
            //super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            //setFragment(new PersonalFragment());
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_receipts) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_about) {
            setFragment(new AboutFragment());
        } else if (id == R.id.nav_contact_us) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
