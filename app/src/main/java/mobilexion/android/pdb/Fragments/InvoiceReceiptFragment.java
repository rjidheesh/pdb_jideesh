package mobilexion.android.pdb.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import mobilexion.android.pdb.Adapters.ListviewAdapter;
import mobilexion.android.pdb.R;

import static mobilexion.android.pdb.Fragments.PersonalFragment.flag;

public class InvoiceReceiptFragment extends Fragment {

    private static final String URL_REQ = "https://epionex.com/ccavRequestHandler";
    private static final String URL_RE_DIRECT = "https://epionex.com/ccavResponseHandler";
    private static final String URL_CANCEL = "https://epionex.com/ccavResponseHandler";

    ListView mItemListView;
    TextView mHeaderTextView, mNumberTextView, mDateTextView, mPayableTextView;
    Button mPayButton;
    Spinner mPayModeSpinner;
    ImageView mPaidImageView;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_invoice_receipt, container, false);

        mHeaderTextView = view.findViewById(R.id.inre_textview_header);
        mNumberTextView = view.findViewById(R.id.inre_textview_number);
        mDateTextView = view.findViewById(R.id.inre_textview_date);
        mItemListView = view.findViewById(R.id.inre_listview_item);
        mPaidImageView = view.findViewById(R.id.inre_imageview_paid);
        mPayableTextView = view.findViewById(R.id.inre_textview_payable_amount);
        mPayModeSpinner = view.findViewById(R.id.inre_spinner_payment_mode);
        mPayButton = view.findViewById(R.id.inre_button_payment);

        setVisibility(mPaidImageView, View.GONE);
        setVisibility(mPayableTextView, View.GONE);
        setVisibility(mPayModeSpinner, View.GONE);
        setVisibility(mPayButton, View.GONE);


        ListviewAdapter adapter = new ListviewAdapter(getActivity(), "1", null);
        mItemListView.setAdapter(adapter);

        switch (flag) {
            case "Invoice":
                mHeaderTextView.setText(flag);
                mNumberTextView.setText(flag+" Number : "+001);
                mDateTextView.setText(flag+" Date : "+"09-05-2019");
                setVisibility(mPayableTextView, View.VISIBLE);
                setVisibility(mPayModeSpinner, View.VISIBLE);
                setVisibility(mPayButton, View.VISIBLE);
                break;
            case "Receipt":
                mHeaderTextView.setText(flag);
                mNumberTextView.setText(flag+" Number : "+001);
                mDateTextView.setText(flag+" Date : "+"09-05-2019");
                setVisibility(mPaidImageView, View.VISIBLE);
                break;
        }

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    void setVisibility(View view, int mode) {
        view.setVisibility(mode);
    }

}
