package mobilexion.android.pdb.Fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import mobilexion.android.pdb.MainActivity;
import mobilexion.android.pdb.R;
import mobilexion.android.pdb.Utility.Util;

import static mobilexion.android.pdb.Utility.Util.FLAG;
import static mobilexion.android.pdb.Utility.Util.MOBILE;


public class OTPFragment extends Fragment {

    private static final String TAG = "Debug";

    private EditText mOTPEditText;
    private Button mVerifyButton;
    private TextView mMobileNumTextView, mResendOTPTextView, mStatusTextView;

    private String mMobileString, mCountryCodeString, mOTPString;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_otp, container, false);

        mOTPEditText = view.findViewById(R.id.otp_edittext_otp);
        mVerifyButton = view.findViewById(R.id.otp_button_verify);
        mMobileNumTextView = view.findViewById(R.id.otp_textview_mobile);
        mResendOTPTextView = view.findViewById(R.id.otp_textview_resend_otp);
        mStatusTextView = view.findViewById(R.id.otp_textview_verification_status);

        mMobileString = getArguments().getString(MobileFragment.ARG_PHONENUMBER);//mobile string
        mCountryCodeString = getArguments().getString(MobileFragment.ARG_COUNTRY_CODE);//country code
        mOTPString = getArguments().getString(MobileFragment.ARG_OTP);//otp

        mMobileNumTextView.setText("Please type the verification code sent to"+"\n"+mMobileString);

        mResendOTPTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });

         mVerifyButton.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {
                 String otp = mOTPEditText.getText().toString();
                 if (otp.length()==4 && otp.equals(mOTPString)) {

                     createPref(true, mMobileString);//updating preference

                     startActivity(new Intent(getActivity(), MainActivity.class).putExtra("MOBILE", mMobileString));
                     getActivity().finish();
                 }
             }
         });

        return view;
    }

    /*private void startTimer() {
        mResendOTPTextView.setClickable(false);
        mResendOTPTextView.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorAlizarin));
        new CountDownTimer(30000, 1000) {
            int secondsLeft = 0;

            public void onTick(long ms) {
                if (Math.round((float) ms / 1000.0f) != secondsLeft) {
                    secondsLeft = Math.round((float) ms / 1000.0f);
                    mResendOTPTextView.setText("Resend via call ( " + secondsLeft + " )");
                }
            }

            public void onFinish() {
                mResendOTPTextView.setClickable(true);
                mResendOTPTextView.setText("Resend via call");
                mResendOTPTextView.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorWhite));
            }
        }.start();
    }*/

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    void createPref(boolean b, String mobile) {
        SharedPreferences.Editor editor = Util.myPreference.edit();
        editor.putBoolean(FLAG, b);
        editor.putString(MOBILE, mobile);
        editor.commit();
    }

}
