package mobilexion.android.pdb.Fragments;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.PermissionRequest;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import mobilexion.android.pdb.R;
import mobilexion.android.pdb.Retrofit.ApiClient;
import mobilexion.android.pdb.Retrofit.ApiInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class WebViewFragment extends Fragment {

    private static final String TAG = "debug_app";

    private WebView mOpenViduWebView;
    private ProgressDialog progressDialog;

    public WebViewFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_web_view, container, false);

        mOpenViduWebView = view.findViewById(R.id.fragment_webview_openvidu);

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Loading");

        getAPI();

        return view;
    }

    void getAPI() {

        progressDialog.show();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<JsonObject> call = apiService.getVcURL();

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                JsonObject object = response.body();

                try {
                    JsonElement vcUrl = object.get("test_base");
                    startVC(vcUrl.getAsString());
                }catch (Exception e){}
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (progressDialog!=null) progressDialog.dismiss();
            }
        });
    }

    void startVC(String url) {

        if (progressDialog!=null) progressDialog.setMessage("Opening VC");

        mOpenViduWebView.setWebViewClient(new MyWebViewClient());
        mOpenViduWebView.getSettings().setJavaScriptEnabled(true);
        mOpenViduWebView.getSettings().setDomStorageEnabled(true);
        mOpenViduWebView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            mOpenViduWebView.getSettings().setMediaPlaybackRequiresUserGesture(false);
        }
        mOpenViduWebView.getSettings().setPluginState(WebSettings.PluginState.ON);
        mOpenViduWebView.loadUrl(url);

        mOpenViduWebView.setWebChromeClient(new WebChromeClient(){
            // Need to accept permissions to use the camera
            @TargetApi(Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onPermissionRequest(final PermissionRequest request) {
                request.grant(request.getResources());
            }

        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {

            if (progressDialog!=null) progressDialog.dismiss();

            final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage("Do you want to continue to this site.");
            builder.setPositiveButton("continue", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    handler.proceed();
                }
            });
            builder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    handler.cancel();
                }
            });
            final AlertDialog dialog = builder.create();
            dialog.show();
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            if (progressDialog!=null) progressDialog.dismiss();
        }
    }
}
