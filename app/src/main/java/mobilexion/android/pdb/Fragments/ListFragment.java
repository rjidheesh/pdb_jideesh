package mobilexion.android.pdb.Fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.io.IOException;
import java.util.List;

import mobilexion.android.pdb.Adapters.ListviewAdapter;
import mobilexion.android.pdb.Pojo.InvoiceReceipt;
import mobilexion.android.pdb.R;
import mobilexion.android.pdb.Retrofit.ApiClient;
import mobilexion.android.pdb.Retrofit.ApiInterface;
import mobilexion.android.pdb.Utility.Util;
import retrofit2.Call;
import retrofit2.Response;

import static mobilexion.android.pdb.Utility.Util.MONGO_ID;

public class ListFragment extends Fragment {

    ListView mSlipListView;
    ProgressDialog mProgressDialog;
    SwipeRefreshLayout mSwipeRefreshLayout;

    List<InvoiceReceipt> listData;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_list, container, false);

        //mProgressDialog = new ProgressDialog(getActivity());

        mSlipListView = view.findViewById(R.id.list_listview_payslip);
        mSwipeRefreshLayout = view.findViewById(R.id.list_swipe_refresh);

        new getFromServer().execute();

        mSlipListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                setFragment(new InvoiceReceiptFragment());
            }
        });

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new getFromServer().execute();
            }
        });

        return view;
    }

    private class getFromServer extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            //mProgressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            Call<List<InvoiceReceipt>> call;

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            String mongoId = Util.myPreference.getString(MONGO_ID, "NA");
            if (PersonalFragment.flag.equals("Invoice")) call = apiService.getInvoice(mongoId);
            else call = apiService.getReceipt(mongoId);
            Response<List<InvoiceReceipt>> response = null;
            try {
                response = call.execute();
            } catch (IOException e) {
                e.printStackTrace();
                return "Error";
            }
            if (response.code() == 200) {
                listData = response.body();
                return "Success";
            }
            else return "Failure";

        }

        @Override
        protected void onProgressUpdate(Void... values) {}

        @Override
        protected void onPostExecute(String result) {
            switch (result) {
                case "Success":
                    ListviewAdapter adapter = new ListviewAdapter(getActivity(), "3", listData);
                    mSlipListView.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                    mSwipeRefreshLayout.setRefreshing(false);
                    break;
                case "Failure":
                    Toast.makeText(getActivity(), "Try again later", Toast.LENGTH_SHORT).show();
                    break;
                case "Error":
                    Toast.makeText(getActivity(), "Server error", Toast.LENGTH_SHORT).show();
                    break;
            }

        }

    }

    private void setFragment(Fragment fragment) {
        this.getFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, fragment)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (mProgressDialog!=null) mProgressDialog.dismiss();
    }

}
