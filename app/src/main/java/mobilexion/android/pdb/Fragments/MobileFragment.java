package mobilexion.android.pdb.Fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import mobilexion.android.pdb.Pojo.Status;
import mobilexion.android.pdb.R;
import mobilexion.android.pdb.Retrofit.ApiClient;
import mobilexion.android.pdb.Retrofit.ApiInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MobileFragment extends Fragment {

    private Button mOTPButton;
    private EditText mMobileEditText;
    private ProgressDialog progressDialog;
    private Spinner mCountrySpinner;

    public static final String ARG_PHONENUMBER = "phonenumber";
    public static final String ARG_COUNTRY_CODE = "code";
    public static final String ARG_OTP = "otp";

    public static String mobileNumber;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_mobile, container, false);

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Verifying...");

        mCountrySpinner = view.findViewById(R.id.mobile_spinner_country_code);
        mMobileEditText = view.findViewById(R.id.mobile_edittext_mobile_no);
        mOTPButton = view.findViewById(R.id.mobile_fab_next);

        mOTPButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {

                mobileNumber = mMobileEditText.getText().toString().replace(" ", "");
                if (mobileNumber.length()!=10) {
                    Toast.makeText(getActivity(), "Enter a valid mobile number", Toast.LENGTH_SHORT).show();
                    return;
                }

                progressDialog.show();//showing progress dialog

                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                Call<Status> call = apiService.verifyMobile(mobileNumber);
                call.enqueue(new Callback<Status>() {
                    @Override
                    public void onResponse(Call<Status> call, Response<Status> response) {
                        try {
                            Status status = response.body();
                            if (status.getStatus().equals("Yes")) {
                                dismissDialog();
                                setFragment(new OTPFragment(), mobileNumber, status.getOtp());
                            } else {
                                dismissDialog();
                                Snackbar.make(view, "Sorry, Please register with Epionex first.", Snackbar.LENGTH_LONG)
                                        .setAction("Action", null).show();
                            }
                        }catch (Exception e) {
                            dismissDialog();
                            Snackbar.make(view, "Try again...", Snackbar.LENGTH_LONG)
                                    .setAction("Action", null).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Status> call, Throwable t) {
                        dismissDialog();
                        Snackbar.make(view, "Some error occured! Try again...", Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();
                    }
                });

            }
        });

        return view;
    }

    void dismissDialog() {
        if (progressDialog.isShowing()) progressDialog.dismiss();
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void setFragment(Fragment fragment, String mobileNo, String Otp) {

        Bundle args = new Bundle();
        args.putString(ARG_PHONENUMBER, mobileNo);
        args.putString(ARG_COUNTRY_CODE, "+91");
        args.putString(ARG_OTP, Otp);
        fragment.setArguments(args);

        this.getFragmentManager().beginTransaction()
                .replace(R.id.login_fragment_conatiner, fragment)
                .addToBackStack(null)
                .commit();
    }
    
}
