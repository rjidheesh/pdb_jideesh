package mobilexion.android.pdb.Fragments;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import mobilexion.android.pdb.Adapters.ListviewAdapter;
import mobilexion.android.pdb.Adapters.RecyclerViewAdapter;
import mobilexion.android.pdb.Pojo.Menu;
import mobilexion.android.pdb.Pojo.Patient;
import mobilexion.android.pdb.R;
import mobilexion.android.pdb.Utility.RecyclerTouchListener;
import mobilexion.android.pdb.Utility.Util;

import static mobilexion.android.pdb.Adapters.ListviewAdapter.itemIconList;
import static mobilexion.android.pdb.Adapters.ListviewAdapter.itemNameList;

public class PersonalFragment extends Fragment {

    private TextView mNameTextView, mAddressTextView, mMobileTextView, mAgeGenderTextView;
    private RecyclerView mMenuRecyclerView;
    private FloatingActionButton mAudioFab, mVideoFab;
    private Button mPayNowButton, mReceiptButton;

    List<Menu> menuList;

    public static String flag;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_personal, container, false);

        mNameTextView = view.findViewById(R.id.personal_textview_name);
        mMobileTextView = view.findViewById(R.id.personal_textview_mobile);
        mAgeGenderTextView = view.findViewById(R.id.personal_textview_age_gender);
        mAddressTextView = view.findViewById(R.id.personal_textview_address);
        mMenuRecyclerView = view.findViewById(R.id.personal_recyclerview_menu);
        mAudioFab = view.findViewById(R.id.personal_fab_audiocall);
        mVideoFab = view.findViewById(R.id.personal_fab_videocall);
        mPayNowButton = view.findViewById(R.id.personal_button_paynow);
        mReceiptButton = view.findViewById(R.id.personal_button_receipt);

        int count = Util.mMyDatabase.dbDao().getCountPatient();

        if (count > 0) {

            Patient patient = Util.mMyDatabase.dbDao().getPatient();

            String gender="_";

            mNameTextView.setText(patient.getName());
            mMobileTextView.setText(patient.getMobileNo());
            try {
                if (patient.getGender() == 1) gender = "Male";
                else gender = "Female";
            } catch (Exception e) {}
            mAgeGenderTextView.setText(patient.getAge()+" | "+gender);
            mAddressTextView.setText(patient.getAddress()+", "+patient.getTaluk()+", "+patient.getPostOffice()
                        +", "+patient.getDistrict()+", PIN-"+patient.getPinCode());
        }

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 3);
        mMenuRecyclerView.setLayoutManager(mLayoutManager);
        mMenuRecyclerView.addItemDecoration(new GridSpacingItemDecoration(3, dpToPx(10), false));
        mMenuRecyclerView.setItemAnimator(new DefaultItemAnimator());

        setAdapter();

        mMenuRecyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), mMenuRecyclerView,
                new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                switch (menuList.get(position).getItem()) {
                    case "Add More":
                        View bottomView = getLayoutInflater().inflate(R.layout.bottom_sheet, null);

                        final BottomSheetDialog dialog = new BottomSheetDialog(getActivity());
                        dialog.setContentView(bottomView);
                        dialog.show();

                        final GridView lv = bottomView.findViewById(R.id.bottonsheet_listview_items);
                        ListviewAdapter adapter = new ListviewAdapter(getActivity(), "2", null);
                        lv.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
                        lv.setAdapter(adapter);

                        Button b = bottomView.findViewById(R.id.bottomsheet_button_add);
                        b.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                ArrayList<Menu> menuList = new ArrayList<Menu>();
                                itemNameList.add("Add More"); itemIconList.add(R.drawable.icons_add_more);
                                for (int i = 0; i< itemNameList.size(); i++)
                                    menuList.add(new Menu(itemNameList.get(i), itemIconList.get(i)));
                                Util.mMyDatabase.dbDao().deletMenu();
                                Util.mMyDatabase.dbDao().addMenu(menuList);
                                Toast.makeText(getActivity(), "Saved!", Toast.LENGTH_SHORT).show();
                                setAdapter();
                                dialog.dismiss();
                            }
                        });
                        break;

                    case "ECG":
                        View views = getLayoutInflater().inflate(R.layout.fragment_otp, null);

                        final BottomSheetDialog dialogs = new BottomSheetDialog(getActivity());
                        dialogs.setContentView(views);
                        dialogs.show();
                        break;

                }
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        mVideoFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setFragment(new WebViewFragment());
            }
        });

        mPayNowButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                flag = "Invoice";
                setFragment(new ListFragment());
            }
        });

        mReceiptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                flag = "Receipt";
                setFragment(new ListFragment());
            }
        });


        return view;
    }

    void setAdapter () {
        if (Util.mMyDatabase.dbDao().getCountMenu() > 0) {
            if (menuList!=null) menuList.clear();
            menuList = Util.mMyDatabase.dbDao().getMenu();
            RecyclerViewAdapter adapter = new RecyclerViewAdapter(getActivity(), menuList);
            mMenuRecyclerView.setAdapter(adapter);
        }else {
            menuList = new ArrayList<Menu>();
            menuList.add(new Menu("Add More", R.drawable.icons_add_more));
            RecyclerViewAdapter adapter = new RecyclerViewAdapter(getActivity(), menuList);
            mMenuRecyclerView.setAdapter(adapter);
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

    private void setFragment(Fragment fragment) {
        /*FragmentManager manager = getActivity().getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.fragment_container, fragment);
        transaction.commit();*/
        this.getFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, fragment)
                .addToBackStack(null)
                .commit();
    }

}
