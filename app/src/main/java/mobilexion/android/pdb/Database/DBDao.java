package mobilexion.android.pdb.Database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import mobilexion.android.pdb.Pojo.Episode;
import mobilexion.android.pdb.Pojo.Menu;
import mobilexion.android.pdb.Pojo.Patient;

@Dao
public interface DBDao {

    @Insert
    void addPatient(Patient patient);

    @Query("SELECT * FROM Patient")
    Patient getPatient();

    @Query("SELECT COUNT(name) FROM Patient")
    int getCountPatient();

    @Insert
    void addEpisode(List<Episode> episode);

    @Query("DELETE FROM Menu")
    void deletMenu();

    @Insert
    void addMenu(List<Menu> menu);

    @Query("SELECT COUNT(item) FROM Menu")
    int getCountMenu();

    @Query("SELECT * FROM Menu")
    List<Menu> getMenu();

  }
