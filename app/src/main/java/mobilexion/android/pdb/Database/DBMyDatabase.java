package mobilexion.android.pdb.Database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import mobilexion.android.pdb.Pojo.Episode;
import mobilexion.android.pdb.Pojo.Menu;
import mobilexion.android.pdb.Pojo.Patient;

@Database(entities = {Patient.class, Episode.class, Menu.class},version = 1,exportSchema = false)
public abstract class DBMyDatabase extends RoomDatabase {
    public abstract DBDao dbDao();

}
